upstream front.app.com {
    server front.app.com:80;
}

upstream backend.app.com {
    server backend.app.com:80;
}

map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

server {
    listen 80;
    server_name app.com;

    client_body_buffer_size 512k;
    proxy_send_timeout   180;
    proxy_read_timeout   180;
    proxy_buffer_size   128k;
    proxy_buffers     16 128k;
    proxy_busy_buffers_size 128k;
    proxy_temp_file_write_size 128k;

    # add_header 'Access-Control-Allow-Origin' 'app.com';
    # add_header 'Host' 'app.com';
    # add_header 'Access-Control-Allow-Credentials' 'true';
    # add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
    # add_header 'Access-Control-Allow-Methods' 'GET, POST, PUT, DELETE, UPDATE';
    
    proxy_connect_timeout 600s;
  
    proxy_set_header   HOST                         $host;
    proxy_set_header   HTTP_HOST                    $host;
    proxy_set_header   SERVER_NAME                  $host;
    proxy_set_header   X-Real-IP                    $remote_addr;
    proxy_set_header   X-Forwarded-For              $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    location / {
        proxy_pass http://front.app.com;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade; #for websockets
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
    }

    location /api/ {
        proxy_pass http://backend.app.com;
    }
}
